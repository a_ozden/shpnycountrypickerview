Pod::Spec.new do |spec|

  spec.name         = "ShpnyCountryPickerView"
  spec.version      = "0.1.2"
  spec.summary      = "A simple, customizable view for selecting countries in iOS apps."
  spec.homepage     = "https://bitbucket.org/a_ozden/shpnycountrypickerview"
  spec.license      = "COMMERCIAL"
  spec.author       = { "Ali Ozden" => "ali.ozden@mobisem.com" }
  spec.platform     = :ios, "10.0"
  spec.source       = { :git => "https://bitbucket.org/a_ozden/shpnycountrypickerview.git", :tag => spec.version }
  spec.source_files  = "CountryPickerView/*.swift"
  spec.resource_bundles = {
    'CountryPickerView' => ['CountryPickerView/Assets/CountryPickerView.bundle/*',
    'CountryPickerView/**/*.{xib}']
  }
  spec.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.2' }

end
